﻿Getting a PC ready for chisel
Optional requirements:
1)  Sublime text
2)  Anaconda

install sublime:

	$ wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
	$ echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
	$ sudo apt-get update
	$ sudo apt-get install sublime-text

install anaconda:
	$ curl -O https://repo.anaconda.com/archive/Anaconda3-2019.03-Linux-x86_64.sh
	$ bash Anaconda3-2019.03-Linux-x86_64.sh
	$ source ~/.bashrc

required stuff:

install jdk(open jdk should work) 11 or 13:
	$ sudo add-apt-repository ppa:linuxuprising/java
	$ sudo apt-get update
	$ sudo apt-get install oracle-java11-installer

set the java path variable, find it using 
	$ update-alternatives --config java
set the path variable:
open /etc/environment:
	$ nano /etc/environment
JAVA_HOME="/your/java/installation-path"

add the path variable and reload it 
	$ source /etc/environment

install sbt
	$ echo "deb https://dl.bintray.com/sbt/debian /" | sudo tee -a /etc/apt/sources.list.d/sbt.list
	$ curl -sL "https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x2EE0EA64E40A89B84B2DF73499E82A75642AC823" | sudo apt-key add
	$ sudo apt-get update
	$ sudo apt-get install sbt

create a python2 environmnet and all of the chisel repos can be gitted in and theyll run out of the box

install dependancies:

	$ sudo apt-get install autoconf automake autotools-dev curl libmpc-dev libmpfr-dev libgmp-dev libusb-1.0-0-dev gawk build-essential bison flex texinfo gperf libtool patchutils bc zlib1g-dev device-tree-compiler pkg-config libexpat-dev

install RISCV-toolchain


