package exercise7
import chisel3._
import chisel3.util._


class Fourbitmux extends Module{
	val io = IO(new Bundle{
	val in0 = Input(Bool())
	val in1 = Input(Bool())
	val in2 = Input(Bool())
	val in3 = Input(Bool())	
	val out = Output(Bool())
	val sel = Input(UInt(2.W))
	})
	val int1 = WireInit(false.B)
	val int2 = WireInit(false.B)
	int1 := Mux(io.sel(0), io.in1, io.in0)
	int2 := Mux(io.sel(0), io.in3, io.in2)
	io.out := Mux(io.sel(1), int2, int1)
}