package exercise3

import chisel3._
//a simple combinational logic example

class Singlebitadder extends Module{
	val io = IO(new Bundle{
		val a 		= Input(Bool())
		val b 		= Input(Bool())
		val sum 	= Output(Bool())
		val carry 	= Output(Bool())
	})
	io.sum := io.a ^ io.b
	io.carry := io.a & io.b
	
}