package exercise9

import chisel3._
import chisel3.util._

class Decoder extends Module{
	val io = IO(new Bundle{
		val in = Input(UInt(2.W))
		val out = Output(UInt(4.W))
	})
	val v0 = (io.in === 0.U)
	val v1 = (io.in === 1.U)
	val v2 = (io.in === 2.U)
	val v3 = (io.in === 3.U)
	io.out := Cat(Seq(v3,v2,v1,v0))
	


	
}