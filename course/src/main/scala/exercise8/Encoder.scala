package exercise8

import chisel3._
import chisel3.util._

class Encoder extends Module{
	val io = IO(new Bundle{
		val in = Input(UInt(4.W))
		val out = Output(UInt(2.W))
	})
	
	val v2 = (io.in >= 4.U)
	val v1 = Mux(v2, io.in >= 8.U, io.in >= 2.U) 
	io.out := Cat(Seq(v2,v1))


	
}