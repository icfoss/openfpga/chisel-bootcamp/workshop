package exercise5

import chisel3._


class Multiplier extends Module{
	val io = IO(new Bundle{
		val a 			= Input(UInt(4.W))
		val b 			= Input(UInt(4.W))
		val product		= Output(UInt(8.W))
	})

	val prod = RegInit(0.U(8.W))
	prod := io.a * io.b
	io.product := prod
}