package exercise10

import chisel3._


class Simplerouter extends Module{
	val io = IO(new Bundle{
		val priority = Input(UInt(2.W))
		val byte = Input(UInt(8.W))
		val low_p = Output(UInt(8.W))
		val normal = Output(UInt(8.W))
		val high_p = Output(UInt(8.W))
	})
	when(io.priority === 1.U){
		io.normal := io.byte
		io.high_p := 0.U
		io.low_p := 0.U
	}.elsewhen(io.priority === 0.U){
		io.normal := 0.U
		io.high_p := 0.U
		io.low_p := io.byte

	}.otherwise{
		io.normal := 0.U
		io.high_p := io.byte
		io.low_p := 0.U
	}
}