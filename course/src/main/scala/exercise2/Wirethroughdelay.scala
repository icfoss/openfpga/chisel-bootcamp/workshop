package exercise2

import chisel3._
//the input node is fed to a register which then drives the output

class Wirethroughdelay extends Module{
	val io = IO(new Bundle{
		val in = Input(Bool())
		val out = Output(Bool())
	})
	val out = RegInit(false.B)
	out := io.in
	io.out := out
}