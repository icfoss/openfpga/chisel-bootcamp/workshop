package exercise6

import chisel3._
class Combmux extends Module{
	val io = IO(new Bundle{
		val in0 = Input(Bool())
		val in1 = Input(Bool())
		val sel = Input(Bool())
		val out = Output(Bool())
	})
	io.out := (io.in0 & !io.sel)| (io.in1 & io.sel)
}