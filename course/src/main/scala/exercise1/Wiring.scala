package exercise1

import chisel3._

//the input is fed to the output
class Wirethrough extends Module{
	val io = IO(new Bundle{
		val in = Input(Bool())
		val out = Output(Bool())
	})
	io.out := io.in
}