package exercise4

import chisel3._
import chisel3.util._
//a simple combinational logic example

class SBA extends Module{
	val io = IO(new Bundle{
		val a 			= Input(Bool())
		val b 			= Input(Bool())
		val carry_in	= Input(Bool())
		val sum 		= Output(Bool())
		val carry_out	= Output(Bool())
	})
	io.sum := io.a ^ io.b ^ io.carry_in
	io.carry_out := (io.a & io.b) | (io.a & io.carry_in) | (io.b & io.carry_in)
}

class RCA extends Module{
	val io = IO(new Bundle{
		val a = Input(UInt(4.W))
		val b = Input(UInt(4.W))
		val sum = Output(UInt(4.W))
		val carry = Output(Bool())
	})
	val sba0 = Module(new SBA)
	val sba1 = Module(new SBA)
	val sba2 = Module(new SBA)
	val sba3 = Module(new SBA)
	sba0.io.a := io.a(0)
	sba1.io.a := io.a(1)
	sba2.io.a := io.a(2)
	sba3.io.a := io.a(3)
	sba0.io.b := io.b(0)
	sba1.io.b := io.b(1)
	sba2.io.b := io.b(2)
	sba3.io.b := io.b(3)
	sba0.io.carry_in := false.B
	sba1.io.carry_in := sba0.io.carry_out
	sba2.io.carry_in := sba1.io.carry_out
	sba3.io.carry_in := sba2.io.carry_out
	io.carry := sba3.io.carry_out
	io.sum := Cat(Seq(sba0.io.sum,sba1.io.sum,sba2.io.sum,sba3.io.sum).reverse)
	
	
	
}