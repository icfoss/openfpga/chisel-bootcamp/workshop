package exercise1

import chisel3._



object WiringMain extends App {
  iotesters.Driver.execute(args, () => new Wirethrough) {
    c => new WirethroughTester(c)
  }
}