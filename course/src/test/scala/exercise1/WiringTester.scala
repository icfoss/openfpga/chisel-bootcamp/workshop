package exercise1


import chisel3.iotesters
import chisel3.iotesters.{ChiselFlatSpec, Driver, PeekPokeTester}

class WirethroughTester(c: Wirethrough) extends PeekPokeTester(c) {

	for(i <- 0 to 40){
		val in = rnd.nextInt(2)
		poke(c.io.in, in)
		expect(c.io.out,in)
	}
}