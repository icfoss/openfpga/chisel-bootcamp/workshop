package exercise4

import chisel3._



object RCAMain extends App {
  iotesters.Driver.execute(args, () => new RCA) {
    c => new RCATester(c)
  }
}