package exercise4


import chisel3.iotesters
import chisel3.iotesters.{ChiselFlatSpec, Driver, PeekPokeTester}

class RCATester(c: RCA) extends PeekPokeTester(c) {

	for(i <- 0 to 40){
		val in1 = rnd.nextInt(4)
		val in2 = rnd.nextInt(4)
		var sum = in1 + in2
		var carry = 0
		if(sum >= 16){
			
			sum = sum - 16
			carry = 1
		}else{
			carry=0
		}
		poke(c.io.a, in1)
		poke(c.io.b, in2)
		expect(c.io.sum, sum)
		expect(c.io.carry, carry)
	}
}