package exercise10

import chisel3._



object SimplerouterMain extends App {
  iotesters.Driver.execute(args, () => new Simplerouter) {
    c => new SimplerouterTester(c)
  }
}