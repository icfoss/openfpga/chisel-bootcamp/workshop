package exercise10


import chisel3.iotesters
import chisel3.iotesters.{ChiselFlatSpec, Driver, PeekPokeTester}

class SimplerouterTester(c: Simplerouter) extends PeekPokeTester(c) {

	for(i <- 0 to 40){
		val byte = rnd.nextInt(256)
		val sel = rnd.nextInt(4)
		poke(c.io.priority, sel)
		poke(c.io.byte, byte)
		if(sel==0){
			expect(c.io.low_p, byte)
			expect(c.io.normal,0)
			expect(c.io.high_p,0)
		}else if(sel==1){
			expect(c.io.low_p, 0)
			expect(c.io.normal,byte)
			expect(c.io.high_p,0)
		}else{
			expect(c.io.low_p, 0)
			expect(c.io.normal,0)
			expect(c.io.high_p,byte)
		}
	}
}