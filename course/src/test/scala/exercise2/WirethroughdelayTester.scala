package exercise2


import chisel3.iotesters
import chisel3.iotesters.{ChiselFlatSpec, Driver, PeekPokeTester}

class WirethroughdelayTester(c: Wirethroughdelay) extends PeekPokeTester(c) {

	for(i <- 0 to 40){
		val in = rnd.nextInt(2)
		poke(c.io.in, in)
		step(1)
		expect(c.io.out,in)
	}
}