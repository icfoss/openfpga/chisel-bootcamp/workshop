package exercise2

import chisel3._



object WirethroughdelayMain extends App {
  iotesters.Driver.execute(args, () => new Wirethroughdelay) {
    c => new WirethroughdelayTester(c)
  }
}