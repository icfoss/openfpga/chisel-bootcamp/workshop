package exercise3

import chisel3._



object SinglebitadderMain extends App {
  iotesters.Driver.execute(args, () => new Singlebitadder) {
    c => new SinglebitadderTester(c)
  }
}