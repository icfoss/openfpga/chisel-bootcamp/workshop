package exercise3


import chisel3.iotesters
import chisel3.iotesters.{ChiselFlatSpec, Driver, PeekPokeTester}

class SinglebitadderTester(c: Singlebitadder) extends PeekPokeTester(c) {

	for(i <- 0 to 40){
		val in1 = rnd.nextInt(2)
		val in2 = rnd.nextInt(2)
		poke(c.io.a, in1)
		poke(c.io.b, in2)
		expect(c.io.sum, in1 ^ in2)
		expect(c.io.carry, in1 & in2)
	}
}