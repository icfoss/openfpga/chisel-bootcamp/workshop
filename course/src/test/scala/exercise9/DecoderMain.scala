package exercise9

import chisel3._



object DecoderMain extends App {
  iotesters.Driver.execute(args, () => new Decoder) {
    c => new DecoderTester(c)
  }
}