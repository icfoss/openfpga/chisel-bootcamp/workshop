package exercise9


import chisel3.iotesters
import chisel3.iotesters.{ChiselFlatSpec, Driver, PeekPokeTester}

class DecoderTester(c: Decoder) extends PeekPokeTester(c) {

	for(i <- 0 to 40){
		val in = rnd.nextInt(4)
		val out = 1 << in
		
		poke(c.io.in, in)
		expect(c.io.out, out)
	}
}