package exercise7

import chisel3._



object FourbitmuxMain extends App {
  iotesters.Driver.execute(args, () => new Fourbitmux) {
    c => new FourbitmuxTester(c)
  }
}