package exercise7


import chisel3.iotesters
import chisel3.iotesters.{ChiselFlatSpec, Driver, PeekPokeTester}

class FourbitmuxTester(c: Fourbitmux) extends PeekPokeTester(c) {

	for(i <- 0 to 40){
		val in0 = rnd.nextInt(2)
		val in1 = rnd.nextInt(2)
		val in2 = rnd.nextInt(2)
		val in3 = rnd.nextInt(2)
		val sel = rnd.nextInt(4)
		val out = if(sel == 0) in0 else if(sel == 1) in1 else if(sel == 2) in2 else in3
		
		poke(c.io.in0, in0)
		poke(c.io.in1, in1)
		poke(c.io.in2, in2)
		poke(c.io.in3, in3)
		poke(c.io.sel, sel)
		expect(c.io.out, out)
	}
}