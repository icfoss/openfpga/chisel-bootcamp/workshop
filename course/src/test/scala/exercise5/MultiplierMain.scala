package exercise5

import chisel3._



object MultiplierMain extends App {
  iotesters.Driver.execute(args, () => new Multiplier) {
    c => new MultiplierTester(c)
  }
}