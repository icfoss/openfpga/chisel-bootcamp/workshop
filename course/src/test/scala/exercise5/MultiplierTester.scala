package exercise5


import chisel3.iotesters
import chisel3.iotesters.{ChiselFlatSpec, Driver, PeekPokeTester}

class MultiplierTester(c: Multiplier) extends PeekPokeTester(c) {

	for(i <- 0 to 40){
		val in1 = rnd.nextInt(4)
		val in2 = rnd.nextInt(4)
		val prod = in1 * in2
		poke(c.io.a, in1)
		poke(c.io.b, in2)
		step(1)
		expect(c.io.product, prod)
	}
}