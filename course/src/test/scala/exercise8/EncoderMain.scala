package exercise8

import chisel3._



object EncoderMain extends App {
  iotesters.Driver.execute(args, () => new Encoder) {
    c => new EncoderTester(c)
  }
}