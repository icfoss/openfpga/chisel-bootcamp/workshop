package exercise8


import chisel3.iotesters
import chisel3.iotesters.{ChiselFlatSpec, Driver, PeekPokeTester}

class EncoderTester(c: Encoder) extends PeekPokeTester(c) {

	for(i <- 0 to 40){
		val out = rnd.nextInt(4)
		val in = 1 << out
		
		poke(c.io.in, in)
		expect(c.io.out, out)
	}
}