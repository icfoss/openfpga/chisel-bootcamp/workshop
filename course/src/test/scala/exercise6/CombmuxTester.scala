package exercise6


import chisel3.iotesters
import chisel3.iotesters.{ChiselFlatSpec, Driver, PeekPokeTester}

class CombmuxTester(c: Combmux) extends PeekPokeTester(c) {

	for(i <- 0 to 40){
		val in1 = rnd.nextInt(2)
		val in2 = rnd.nextInt(2)
		val sel = rnd.nextInt(2)
		val out = if(sel == 0) in1 else in2
		
		poke(c.io.in0, in1)
		poke(c.io.in1, in2)
		poke(c.io.sel, sel)
		expect(c.io.out, out)
	}
}