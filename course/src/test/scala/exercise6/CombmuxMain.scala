package exercise6

import chisel3._



object CombmuxMain extends App {
  iotesters.Driver.execute(args, () => new Combmux) {
    c => new CombmuxTester(c)
  }
}