# Workshop

Documentation, guides and reference for the bootcamp


# Day 1

## Requirements:

	1)Git
	2)Curl
	3)Sublime text / any other text editor
	4)Anaconda (for Python 2.7)
	5)Open JDK (11 or older)
	6)GTKWave

## Install Git:

	$ sudo apt install git

## Install Curl:

	$ sudo apt install curl

## Install Sublime Editor:

	$ wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
	$ echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
	$ sudo apt-get update
	$ sudo apt-get install sublime-text

## Install anaconda:

	$ curl -O https://repo.anaconda.com/archive/Anaconda3-2019.03-Linux-x86_64.sh
	$ bash Anaconda3-2019.03-Linux-x86_64.sh
	$ source ~/.bashrc

## Activate Python 2.7 :
	
	$ conda create -n chipenv python=2.7
	$ conda activate chipenv

## Install OpenJDK :

* **If multiple versions are installed use the following command to change it to a version lower than 11**

	$ sudo update-alternatives --config java

* **Easy Method**

	$ sudo apt install openjdk-8-jdk

* **Other Method** (if Easy Method does not work)
	
	$ sudo add-apt-repository ppa:linuxuprising/java
	$ sudo apt-get update
	$ sudo apt-get install oracle-java11-installer

	Set the path variable:
	open /etc/environment:

		$ nano /etc/environment
		$ JAVA_HOME="/your/java/installation-path"

	Add the path variable and reload it 

		$ source /etc/environment

## Install sbt:

	$ echo "deb https://dl.bintray.com/sbt/debian /" | sudo tee -a /etc/apt/sources.list.d/sbt.list
	$ curl -sL "https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x2EE0EA64E40A89B84B2DF73499E82A75642AC823" | sudo apt-key add
	$ sudo apt-get update
	$ sudo apt-get install sbt


## Install GTKWave:
	
	$ sudo apt install gtkwave


## Setup Chisel Template to get started with Chisel:

	$ git clone https://github.com/freechipsproject/chisel-template.git


# End of Day 1


------------------------------------------------------------------------------------------------------------------------------------------


Install dependancies for Chipyard:

	$ sudo apt-get install autoconf automake autotools-dev curl libmpc-dev libmpfr-dev libgmp-dev libusb-1.0-0-dev gawk build-essential bison flex texinfo gperf libtool patchutils bc zlib1g-dev device-tree-compiler pkg-config libexpat-dev

Install RISCV-toolchain
